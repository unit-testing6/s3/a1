const names = {
	"Brandon" : {
		"name" : "Brandon Boyd",
		"age" : 35
	},
	"Steve": {
		"name" : "Steve Tyler",
		"age" : 56
	}

}


function factorial(n){
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
}

module.exports = {
	factorial: factorial,
	names: names
}
